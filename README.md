# Scenario 1 - Terraform, AWS, CICD:
## Components:
- Primary AWS Resources:
  - Load balancer
  - EC2 instances in Auto scaling group as Target group
  - vpc+subnets(public,private setup)
  - S3 bucket having the static site source code
  - 1 Nat gateway for instances in private network to communicate with yum repo and S3
- Terraform with remote backend in S3
- gitlab CI connected to AWS account using web identity

## Setup
- `tf-backend` folder has the `S3` backend config. Run followings to grab bucket name from output:
  - `terraform init`
  - `terraform apply`
- `tf-infra` has the infrastructure coded in HCL. To run in indenpendently, a S3 bucket name is required as remote backend config which can be collected from previous step. It has following outputs:
  - `gitlab_oidc_role` which is required to connect `gitlab-ci` with AWS based on web identity. Details here https://docs.gitlab.com/ee/integration/openid_connect_provider.html
  - `s3_site_bucket` which has the website source code
  - `loadbalancer_dns_name` which is the loadbalancer default dns record
- `.gitlab-ci.yml` is configured on all branches having following config:
  - `terraform apply` only runs only on `master` branch
  - `terraform plan` runs on rest of the branches to check change impact

## Instance refresh:

Instance refresh happens at following cases:
- if there is any changes in `launch_template`, `mixed_instances_policy` from terraform infra code 
- if `site/index.html` changes, which has the website src code linked to auto-scaling group at instance tag level changes https://gitlab.com/k.hasan.rajib/scenario-1/-/blob/master/tf-infra/autoscaling-grp.tf#L41

## Deliverables:

- Source code: https://gitlab.com/k.hasan.rajib/scenario-1
- Sample Pipeline which uppercased site content:
  - Pipeline: https://gitlab.com/k.hasan.rajib/scenario-1/-/pipelines/673506529
  - Merge request: https://gitlab.com/k.hasan.rajib/scenario-1/-/merge_requests/6/diffs

  
![](uppercased.png) 
