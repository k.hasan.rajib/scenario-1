module "alb" {
  source                         = "cloudposse/alb/aws"
  version                        = "1.5.0"
  vpc_id                         = module.vpc.vpc_id
  security_group_ids             = [module.vpc.vpc_default_security_group_id]
  subnet_ids                     = module.subnets.public_subnet_ids
  target_group_port              = var.target_group_port
  target_group_target_type       = "instance"
  target_group_name              = "${module.this.id}-tg"
  http_enabled                   = true
  access_logs_enabled            = false
  health_check_healthy_threshold = 3
  tags                           = module.this.tags
}