terraform {
  backend "s3" {
    bucket = "counter-cloud-tf-backend-dodo"
    region = "ap-southeast-1"
    key    = "terraform.tfstate"
  }
}