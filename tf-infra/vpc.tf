locals {
  availability_zones = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
}
module "vpc" {
  source     = "cloudposse/vpc/aws"
  version    = "1.2.0"
  name       = "${module.this.id}-vpc"
  cidr_block = "10.0.0.0/16"
  context    = module.this.context
}

module "subnets" {
  source              = "cloudposse/dynamic-subnets/aws"
  version             = "2.0.4"
  name                = "${module.this.id}-subnet"
  availability_zones  = local.availability_zones
  vpc_id              = module.vpc.vpc_id
  igw_id              = [module.vpc.igw_id]
  nat_gateway_enabled = true
  max_nats            = 1
  context             = module.this.context
}