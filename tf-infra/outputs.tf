output "s3_site_bucket" {
  value = aws_s3_bucket.s3_site_bucket.id
}
output "loadbalancer_dns_name" {
  value = module.alb.alb_dns_name
}

output "gitlab_oidc_role" {
  value = module.gitlab_oidc_role.arn
}