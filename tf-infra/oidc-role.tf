resource "aws_iam_openid_connect_provider" "gitlab_oidc" {
  url = "https://gitlab.com"

  client_id_list = [
    "https://gitlab.com"
  ]

  thumbprint_list = ["b3dd7606d2b5a8b4a13771dbecc9ee1cecafa38a"]
}


module "gitlab_oidc_role" {
  source  = "cloudposse/iam-role/aws"
  version = "0.16.2"

  policy_description = "EC2 autoscaling deployment access"
  role_description   = "IAM role with permissions to perform actions EC2,autscaling,vpc ect on behalf a web identity"

  principals = {
    Federated = [aws_iam_openid_connect_provider.gitlab_oidc.arn]
  }
  assume_role_conditions = [

    {
      test     = "StringEquals"
      variable = "gitlab.com:aud"

      values = [
        "https://gitlab.com"
      ]
    }
  ]
  assume_role_actions = ["sts:AssumeRoleWithWebIdentity"]
  #   managed_policy_arns = [data.aws_iam_policy.admin.arn]
  policy_documents = [
    data.aws_iam_policy_document.admin-policy.json
  ]
  context = module.this.context
  name    = "${module.this.id}-oidc-role"
}

data "aws_iam_policy_document" "admin-policy" {
  statement {
    actions   = ["*"]
    resources = ["*"]
  }

}