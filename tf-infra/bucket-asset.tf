resource "random_pet" "site_bucket_suffix_random" {
  keepers = {
    bucket_name = var.project_name
  }
  length = 1
}

resource "aws_s3_bucket" "s3_site_bucket" {
  bucket = "${var.project_name}-site-${random_pet.site_bucket_suffix_random.id}"
}

resource "aws_s3_bucket_versioning" "s3_site_bucket_versioning" {
  bucket = aws_s3_bucket.s3_site_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}