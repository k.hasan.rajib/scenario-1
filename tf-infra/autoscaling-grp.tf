locals {
  userdata = <<-USERDATA
    #!/bin/bash
    yum update -y
    amazon-linux-extras install -y nginx1
    systemctl start nginx
    systemctl enable nginx
    rm -rf /usr/share/nginx/html/*
    aws s3 cp s3://counter-cloud-site-mammal/index.html /usr/share/nginx/html/
    chown -R nginx:nginx /usr/share/nginx/html/*
  USERDATA
}

module "autoscale_group" {
  source  = "cloudposse/ec2-autoscale-group/aws"
  version = "0.30.1"

  image_id                    = "ami-0f62d9254ca98e1aa"
  instance_type               = "t3a.micro"
  security_group_ids          = [module.vpc.vpc_default_security_group_id, aws_security_group.allow_http.id]
  subnet_ids                  = module.subnets.private_subnet_ids
  health_check_type           = "EC2"
  min_size                    = 3
  desired_capacity            = 3
  max_size                    = 3
  wait_for_capacity_timeout   = "5m"
  associate_public_ip_address = false
  user_data_base64            = base64encode(local.userdata)
  iam_instance_profile_name   = aws_iam_instance_profile.instance_profile.id
  # All inputs to `block_device_mappings` have to be defined
  target_group_arns = [module.alb.default_target_group_arn]
  instance_refresh = {
    strategy = "Rolling"
    triggers = ["tag", "tags"]
    preferences = {
      min_healthy_percentage = 50
      instance_warmup        = 120
    }
  }
  tags = {
    Name     = module.this.id
    sitehash = sha1(file("../site/index.html"))
  }
  block_device_mappings = [
    {
      device_name  = "/dev/sda1"
      no_device    = "false"
      virtual_name = "root"
      ebs = {
        volume_size           = 10
        delete_on_termination = true
        encrypted             = true
        volume_type           = "standard"
        iops                  = null
        kms_key_id            = null
        snapshot_id           = null
      }
    }
  ]

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled = false
  enable_monitoring            = false
  context                      = module.this.context
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description     = "HTTP from Loadbalancer"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [module.alb.security_group_id]
  }
  ingress {
    # for_each = toset(module.subnets.private_subnet_cidrs)
    description = "HTTP from subnet"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = module.subnets.private_subnet_cidrs
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = module.this.tags
}

data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "instance" {
  name                = "${module.this.id}-instance-role"
  path                = "/system/"
  assume_role_policy  = data.aws_iam_policy_document.instance-assume-role-policy.json
  managed_policy_arns = [aws_iam_policy.site_bucket_policy.arn]
}

resource "aws_iam_policy" "site_bucket_policy" {
  name   = "${module.this.id}-site-bucket-policy"
  path   = "/"
  policy = data.aws_iam_policy_document.site_bucket.json
}


data "aws_iam_policy_document" "site_bucket" {
  statement {
    sid       = "S3GetobjectForInstance"
    actions   = ["s3:GetObject", "s3:ListAllMyBuckets", "s3:GetBucketLocation", "s3:ListBucket"]
    resources = ["${aws_s3_bucket.s3_site_bucket.arn}/*", aws_s3_bucket.s3_site_bucket.arn]
  }
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "${module.this.id}-instance-role"
  role = aws_iam_role.instance.name
}

resource "null_resource" "cluster" {
  triggers = {
    site_index_hash = sha1(file("../site/index.html"))
    bucket          = aws_s3_bucket.s3_site_bucket.id
  }

  provisioner "local-exec" {

    command = "aws s3 cp ../site/index.html s3://${aws_s3_bucket.s3_site_bucket.id}"
  }
  depends_on = [
    aws_s3_bucket.s3_site_bucket
  ]
}