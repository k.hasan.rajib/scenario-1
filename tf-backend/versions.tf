terraform {
  required_version = ">=1.1,<=2.0.0"
  required_providers {
    aws = {
      version = ">=4.2,<5.0"
      source  = "hashicorp/aws"
    }
    random = {
      source  = "hashicorp/random"
      version = ">=3.1"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
}